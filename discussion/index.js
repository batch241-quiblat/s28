// CRUD Operations

/*
	CRUD
	- Create, Read, Update, Delete
	- is the heart of any backend application
	- mongo DB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods
*/


// CREATE: Inserting documents


// INSERT ONE
/*
	- syntax:
		db.collectionName.insertOne({})

	Inserting/Assigning values in JS Objects:
		object.object.method({object})
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "12345",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


// INSERT MANY
/*
	- syntax:
	db.users.insertMany([
	{objectA},{objectB}
	])
*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 26,
	contact: {
		phone: "123456",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "654321",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "none"
}
]);


// READ: FINDING DOCUMENTS

// FIND/DISPLAY ALL
/*
	- syntax:
		db.collection.find();
*/
db.users.find();



// Finding Users with single arguments
/*
	Syntax:
	db.collectionName.find({field:value})
*/

//Looking for stephen hawking using firstname
db.users.find({firstname: "Stephen"});

//finding users with multiple arguments

//with match
db.users.find({firstName:"Stephen", age:20});


// UPDATE: Updating Documents
// Repeat Jane to update

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:{
			firstName: "Jane"
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "111111",
				email: "janedoe@gmail.com"
			},
			courses: ["AWS", "Google CLoud", "Azure"],
			department: "infrastracture",
			status:"active"
		}
	}
);


// UPDATE ONE
/*
	- SYntax:
		db.collectionName.updateOne({criteria}, {$set: (field: "value")});
*/
db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:{
			firstName: "Jane"
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "111111",
				email: "janedoe@gmail.com"
			},
			courses: ["AWS", "Google CLoud", "Azure"],
			department: "infrastracture",
			status:"active"
		}
	}
);


//UPDATE MANY
db.users.updateMany(
	{department:"none"}, {
		$set: {
			department:"HR"
		}
	}
)
db.users.find().pretty();

// REPLACE ONE
/*
	- can be used if replacing the whole document is necessary
	- syntax:
		db.collectionName.replaceOne(
		{criteria}, {
			$set:{ field:value}
		}
		)
*/
db.users.replaceOne(
	{lastName:"Gates"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Clinton"
		}
	}
);
db.users.find({firstName: "Bill"});


// DELETE : Deleting Documents
/*
	- 
*/

db.users.insert ({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.find({firstName: "Test"});

//deleting a single document
/*
	- syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deleteOne({age: 26});

// Delete Many
db.users.deleteMany({
	courses: [],
})
db.users.find();

// delete all
// db.users.delete();
/*
	- Be careful when using the delete() method. If no search criteria is provided will delete all the documents
	- do not use: db.collectionName.deleteMany()
	- SYntax:
		db.collectionName.deleteMany({criteria});
*/

// ADVANCE QUERIES

// Query an embedded document

db.users.find({
	contact : {
        email : "stephenhawking@gmail.com"
    }
});

// Find the document with the email "janedoe@gmail.com"
//Querying on nested fields
db.users.find({"contact.email": "janedoe@gmail.com"});


//Querying an Array with exact elements
db.users.find({courses: ["React", "Laravel", "SASS"]});

//querying documents regardless of order
db.users.find({courses: {$all:[ "React", "SASS", "Laravel"]}});


//Make an array to query
db.users.insert({
	nameArr: [
	{
		nameA: "Juan"
	},
	{
		nameB: "Tamad"
	}]
});


//
db.users.find({
	nameArr: {
		nameA: "Juan"
	}
});


